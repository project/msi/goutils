package transliteration

import (
	"testing"
)

func TestRuEnOk(t *testing.T) {
	data := "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЫыЪъЬьЭэЮюЯя @#$€%&?|!(){}<>=*+-_:;,.0123456789"
	expected := "AaBbVvGgDdEeEeZhzhZzIiIiKkLlMmNnOoPpRrSsTtUuFfKhkhTstsChchShshShchshchYyIeieEeIuiuIaia @#$€%&?|!(){}<>=*+-_:;,.0123456789"
	result, _ := RuEn889(data)
	if result != expected {
		t.Error("Ru to en transliteration failed")
	}
}

func TestRuEnFaile(t *testing.T) {
	data := "人"
	expected := ""
	result, err := RuEn889(data)
	t.Log(result)
	if err.Error() != "人 contains unsupported characters" || result != expected {
		t.Error("Ru to en transliteration failed")
	}
}
