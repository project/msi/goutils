package set

import (
	"fmt"
)

// Set структура данных.
type Set[eType comparable] struct {
	Elements map[eType]struct{}
}

// NewSet возвращает готовую к работе структуру данных Set.
func NewSet[eType comparable]() *Set[eType] {
	return &Set[eType]{Elements: make(map[eType]struct{})}
}

// Add добавляет новый элемент.
func (s *Set[eType]) Add(elems ...eType) {
	for _, elem := range elems {
		s.Elements[elem] = struct{}{}
	}
}

// Delete удаляет элемент.
func (s *Set[eType]) Delete(elem eType) {
	delete(s.Elements, elem)
}

// Contains проверяет наличие элемента.
func (s *Set[eType]) Contains(elem eType) bool {
	_, exists := s.Elements[elem]
	return exists
}

// Len возвращает число элементов в структуре.
func (s *Set[eType]) Len() int {
	return len(s.Elements)
}

// ElementAsString возвращает элемент в виде строки.
// Если элемента нет - возвращает пустую строку и ошибку.
func (s *Set[eType]) ElementAsString(elem eType) (string, error) {
	if s.Contains(elem) {
		return fmt.Sprintf("%v", elem), nil
	}
	return "", fmt.Errorf("%v element not found", elem)
}

// ElementsAsSlice возвращает элементы как slice.
func (s *Set[eType]) ElementsAsSlice() []eType {
	keysSlice := make([]eType, 0, len(s.Elements))
	for key := range s.Elements {
		keysSlice = append(keysSlice, key)
	}
	return keysSlice
}
