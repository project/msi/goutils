package set

import (
	"slices"
	"testing"
)

func TestSetAdd(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")

	if _, ok := set.Elements["foo"]; ok {
		return
	}
	t.Error("Test Set.Add function failed")
}

func TestSetAddMany(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")
	set.Add("foo")
	set.Add("foo")
	set.Add("foo")

	if len(set.Elements) != 1 {
		t.Error("Test many elements Set.Add function failed")
	}
}

func TestSetContains(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")

	if set.Contains("foo") {
		return
	}
	t.Error("Test Set.Contains function failed")
}

func TestSetDelete(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")
	set.Delete("foo")

	if _, ok := set.Elements["foo"]; ok {
		t.Error("Test Set.Delete function failed")
	}
}

func TestSetLen(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")
	if set.Len() != 1 {
		t.Error("Test Set.Len function failed")
	}

	set.Add("bar")
	if set.Len() != 2 {
		t.Error("Test Set.Len function failed")
	}
}

func TestSetElementAsString(t *testing.T) {
	set := NewSet[int]()
	set.Add(10)

	strEl, err := set.ElementAsString(10)
	if err != nil {
		t.Error("Test Set.ElementAsString function failed ", err)
	}

	if strEl != "10" {
		t.Error("Test Set.ElementAsString function failed")
	}

	_, err = set.ElementAsString(11)
	if err == nil {
		t.Error("Test Set.ElementAsString function failed ", err)
	}
}

func TestSetElementsAsSlice(t *testing.T) {
	set := NewSet[string]()
	set.Add("foo")
	set.Add("bar")

	slElem := set.ElementsAsSlice()

	if !slices.Contains(slElem, "foo") {
		t.Error("Test Set.ElementsAsSlice function failed")
	}

	if !slices.Contains(slElem, "bar") {
		t.Error("Test Set.ElementsAsSlice function failed")
	}
}
