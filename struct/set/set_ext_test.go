package set

import (
	"bytes"
	"slices"
	"testing"
)

func TestSetExtAdd(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")

	if _, ok := set.Elements["foo"]; ok {
		return
	}
	t.Error("Test SetExt.Add function failed")
}

func TestSetExtAddMany(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("foo")
	set.Add("foo")
	set.Add("foo")

	if len(set.Elements) != 1 {
		t.Error("Test many elements SetExt.Add function failed")
	}
}

func TestSetExtContains(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")

	if set.Contains("foo") {
		return
	}
	t.Error("Test SetExt.Contains function failed")
}

func TestSetExtDelete(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Delete("foo")

	if _, ok := set.Elements["foo"]; ok {
		t.Error("Test SetExt.Delete function failed")
	}
}

func TestSetExtLen(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	if set.Len() != 1 {
		t.Error("Test SetExt.Len function failed")
	}

	set.Add("bar")
	if set.Len() != 2 {
		t.Error("Test SetExt.Len function failed")
	}
}

func TestSetExtElementAsString(t *testing.T) {
	set := NewSetExt[int]()
	set.Add(10)

	strEl, err := set.ElementAsString(10)
	if err != nil {
		t.Error("Test SetExt.ElementAsString function failed ", err)
	}

	if strEl != "10" {
		t.Error("Test SetExt.ElementAsString function failed")
	}

	_, err = set.ElementAsString(11)
	if err == nil {
		t.Error("Test SetExt.ElementAsString function failed ", err)
	}
}

func TestSetExtElementsAsSlice(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("bar")

	slElem := set.ElementsAsSlice()

	if !slices.Contains(slElem, "foo") {
		t.Error("Test SetExt.ElementsAsSlice function failed")
	}

	if !slices.Contains(slElem, "bar") {
		t.Error("Test SetExt.ElementsAsSlice function failed")
	}
}

func TestSetExtReverce(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	reverce := set.Reverce()

	if val, ok := reverce[1]; ok {
		if val[0] != "foo" {
			t.Error("Test SetExt.Reverce function failed")
		}
	}
}

func TestSetExtSortedLt(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("bar")
	set.Add("foo")

	var buffer bytes.Buffer

	set.SortedLt(&buffer)

	// t.Log(buffer.String())
	if buffer.String() != "1: [bar]\n2: [foo]\n" {
		t.Error("Test SetExt.SortedLt function failed")
	}
}

func TestSetExtSortedGt(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("bar")
	set.Add("foo")

	var buffer bytes.Buffer

	set.SortedGt(&buffer)

	// t.Log(buffer.String())
	if buffer.String() != "2: [foo]\n1: [bar]\n" {
		t.Error("Test SetExt.SortedGt function failed")
	}
}

func TestSetExtSortedElementsLt(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("bar")
	set.Add("foo")

	sorted := set.SortedElementsLt()

	// t.Log(sorted)
	if sorted[0] != "bar" || sorted[1] != "foo" {
		t.Error("Test SetExt.SortedElementsLt function failed")
	}
}

func TestSetExtSortedElementsGt(t *testing.T) {
	set := NewSetExt[string]()
	set.Add("foo")
	set.Add("bar")
	set.Add("foo")

	sorted := set.SortedElementsGt()

	// t.Log(sorted)
	if sorted[0] != "foo" || sorted[1] != "bar" {
		t.Error("Test SetExt.SortedElementsGt function failed")
	}
}
