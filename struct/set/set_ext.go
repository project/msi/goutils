package set

import (
	"fmt"
	"io"
	"slices"
	"sort"
)

// SetExt расширенная версия структуры данных Set.
// Ведет подсчет, сколько раз элемент был добавлен в структуру.
type SetExt[eType comparable] struct {
	Elements map[eType]int
}

// NewSetExt возвращает готовую к работе структуру данных SetExt.
func NewSetExt[eType comparable]() *SetExt[eType] {
	return &SetExt[eType]{Elements: make(map[eType]int)}
}

// Add добавляет новый элемент.
// Если элемент уже есть, увеличивает его счетчик (значение).
func (s *SetExt[eType]) Add(elems ...eType) {
	for _, elem := range elems {
		if val, ok := s.Elements[elem]; ok {
			s.Elements[elem] = val + 1
			return
		}
		s.Elements[elem] = 1
	}
}

// Delete удаляет элемент.
func (s *SetExt[eType]) Delete(elem eType) {
	delete(s.Elements, elem)
}

// Contains проверяет наличие элемента.
func (s *SetExt[eType]) Contains(elem eType) bool {
	_, exists := s.Elements[elem]
	return exists
}

// Len возвращает число элементов в структуре.
func (s *SetExt[eType]) Len() int {
	return len(s.Elements)
}

// ElementAsString возвращает элемент в виде строки.
// Если элемента нет - возвращает пустую строку и ошибку.
func (s *SetExt[eType]) ElementAsString(elem eType) (string, error) {
	if s.Contains(elem) {
		return fmt.Sprintf("%v", elem), nil
	}
	return "", fmt.Errorf("%v element not found", elem)
}

// ElementsAsSlice возвращает элементы как slice.
func (s *SetExt[eType]) ElementsAsSlice() []eType {
	keysSlice := make([]eType, 0, len(s.Elements))
	for key := range s.Elements {
		keysSlice = append(keysSlice, key)
	}
	return keysSlice
}

// Reverce меняет местами элемент и количество его использования.
// Может быть использовано для сортировки элементов.
func (s *SetExt[eType]) Reverce() map[int][]eType {
	result := map[int][]eType{}
	for k, v := range s.Elements {
		if val, ok := result[v]; ok {
			result[v] = append(val, k)
		} else {
			result[v] = []eType{k}
		}
	}
	return result
}

// SortedLt сортирует элементы по количеству использования об меньшего к большему
// и передает на обруботку интерфейсу io.Writer.
// Выводит информацию о количестве использованя.
func (s *SetExt[eType]) SortedLt(writer io.Writer) {
	revElements := s.Reverce()

	keysSlice := make([]int, 0, len(revElements))
	for key := range revElements {
		keysSlice = append(keysSlice, key)
	}

	slices.Sort(keysSlice) // сортирует в порядке возрастания

	for _, key := range keysSlice {
		str := fmt.Sprintf("%d: %v\n", key, revElements[key])
		writer.Write([]byte(str))
	}
}

// SortedGt сортирует элементы по количеству использования от большего
// к меньшему и передает на обруботку интерфейсу io.Writer.
// Выводит информацию о количестве использованя.
func (s *SetExt[eType]) SortedGt(writer io.Writer) {
	revElements := s.Reverce()

	keysSlice := make([]int, 0, len(revElements))
	for key := range revElements {
		keysSlice = append(keysSlice, key)
	}

	sort.Slice(keysSlice, func(i, j int) bool { // сортирует в порядке убывания
		return keysSlice[i] > keysSlice[j]
	})

	for _, key := range keysSlice {
		str := fmt.Sprintf("%d: %v\n", key, revElements[key])
		writer.Write([]byte(str))
	}
}

// SortedElementsLt сортирует элементы по количеству использования от меньшего к большему.
func (s *SetExt[eType]) SortedElementsLt() []eType {
	revElements := s.Reverce()

	keysSlice := make([]int, 0, len(revElements))
	for key := range revElements {
		keysSlice = append(keysSlice, key)
	}

	slices.Sort(keysSlice) // сортирует в порядке возрастания

	result := make([]eType, 0, len(s.Elements))
	for _, key := range keysSlice {
		result = append(result, revElements[key]...)
	}
	return result
}

// SortedElementsLt сортирует элементы по количеству использования от большего к меньшему
func (s *SetExt[eType]) SortedElementsGt() []eType {
	revElements := s.Reverce()

	keysSlice := make([]int, 0, len(revElements))
	for key := range revElements {
		keysSlice = append(keysSlice, key)
	}

	sort.Slice(keysSlice, func(i, j int) bool { // сортирует в порядке убывания
		return keysSlice[i] > keysSlice[j]
	})

	result := make([]eType, 0, len(s.Elements))
	for _, key := range keysSlice {
		result = append(result, revElements[key]...)
	}

	return result
}
